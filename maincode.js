
document.addEventListener("DOMContentLoaded", function (){
var cvs = document.getElementById("canvas");
var ctx = cvs.getContext("2d");
cvs.width = 600;
cvs.height = 450;
for (let i=150; i<cvs.width; i = i + 150){
    ctx.beginPath();
    x = i;
    ctx.lineWidth = 2;
    ctx.strokeStyle = "black";
    ctx.moveTo(x,0);
    ctx.lineTo(x,450);
    ctx.stroke();
    ctx.closePath();
}

for (let i=150; i<cvs.height; i = i + 150){
    ctx.beginPath();
    y = i;
    ctx.lineWidth = 2;
    ctx.strokeStyle = "black";
    ctx.moveTo (0,y);
    ctx.lineTo(600,y);
    ctx.stroke();
    ctx.closePath();
}

})
